- # cade2-example

This is a example Resouces folder with google sheet feature and new logic for the CADE2 pathway-based simulation tool.


- If you don't already have Docker on your system, download and install it from https://store.docker.com/search?type=edition&offering=community

- If you don't already have Git on your system, download and install it from https://git-scm.com/downloads

- Clone this project

`git clone https://gitlab.com/KseniyaBorokhovskaya/cade2-example.git`

There is a docker-compose file to run the simulation using the following command from within the cade-gest-diab3 directory:

`docker-compose up`

**Google sheets feature usage**

_Before usage:_
1. Auth - use [guide](https://gitlab.com/KseniyaBorokhovskaya/cade2-example/-/blob/master/Guide%20google%20sheets.pages) to get auth key and place it in resource folder. Specify path in config.py, “path_sheets_auth_key” attribute. (Example: “resources/your_key_file.json”)
2. Set an email you are going to use to explore information in google sheets. (In config.py: “email_sheets” attribute)

_Cases:_
1. Attribute ”is_log_to_google_cloud” is true to log to google cloud(main logs, conversion logs and andvalidate logs) and google sheet (event log). Specify sheet_name attribute if you want to save data in specific spreadsheet, for example the same the connections info are saved to.
    1. Open https://console.cloud.google.com/logs/ to see main logs. Should contain all logs as in 20210406T211149 mainLog.log, 20210406T211149 validateLog.log and 20210406T211149 conversionLog.log files.
    2. Open google sheet link (you can follow the link printed in terminal or in main log). Should contain all logs as in eventLog.csv file.
2. Attribute ”is_running_connections” is true to run connections (including google sheet connection). Connections are specified in extensions.py and attributeSet.csv files.


extensions.py connection example with some explanations (use in attributeSet.csv file, Connection attribute):

```
def google_sheet2():
    return [{
        'name': 'google_sheet2', # each connection should be provided with unique name, otherwise connection will be overwritten 
        'type': 'google_sheets', # specify connection type here (“google_sheets”, “fhir”)
        'sheet_name': 'some new name', 
        'worksheet_name': 'specimen',
        'mode': SheetsModes.APPEND, # append or override mode (SheetsModes.OVERRIDE)
        'rows_in_bunch': 2, # used for speeding up writing data to google sheet
        'email': ‘your_email@gmail.com' 
    }]
```


