<Organization>
    <id value="41d7ff89-2bc0-494b-99d0-3ac05ba8553a"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Organization-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/ods-organization-code"/>
        <value value="D82015"/>
    </identifier>
    <!--
    <name value="Manor GP Practice"/>
    <address>
        <line value="30/32 Bridge Street"/>
        <city value="Downham Market"/>
        <district value="Norfolk"/>
        <postalCode value="PE38 9DH"/>
    </address>
    -->
</Organization>