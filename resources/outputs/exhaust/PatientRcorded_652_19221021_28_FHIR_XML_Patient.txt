<Patient>
    <id value="b2d5c698-178e-48e0-a2e8-b98303ad149e"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Patient-1"/>
    </meta>
    <identifier>
        <extension url="https://fhir.hl7.org.uk/STU3/StructureDefinition/Extension-CareConnect-NHSNumberVerificationStatus-1">
            <valueCodeableConcept>
                <coding>
                    <system value="https://fhir.hl7.org.uk/STU3/CodeSystem/CareConnect-NHSNumberVerificationStatus-1"/>
                    <code value="01"/>
                    <display value="Number present and verified"/>
                </coding>
            </valueCodeableConcept>
        </extension>
        <system value="https://fhir.nhs.uk/Id/nhs-number"/>
        <value value="7832003812"/>
    </identifier>
    <name>
        <use value="official"/>
        <family value="Marsh"/>
        <given value="Anna"/>
    </name>
</Patient>