<Organization>
    <id value="21ed1435-4412-45c6-a0d2-2a960d491b4b"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Organization-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/ods-organization-code"/>
        <value value="TKD13"/>
    </identifier>
    <!--
    <name value="Midtown District Hospital"/>
    <address>
        <line value="Beckett Street"/>
        <city value="Leeds"/>
        <district value="West YorkShire"/>
        <postalCode value="LS9 7TF"/>
    </address>
    -->
</Organization>