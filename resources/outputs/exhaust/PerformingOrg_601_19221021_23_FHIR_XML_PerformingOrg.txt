<Organization>
    <id value="bd8e7e53-9c0d-4f92-b6e2-753f14cb910a"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Organization-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/ods-organization-code"/>
        <value value="TKD13"/>
    </identifier>
    <!--
    <name value="Midtown District Hospital"/>
    <address>
        <line value="Beckett Street"/>
        <city value="Leeds"/>
        <district value="West YorkShire"/>
        <postalCode value="LS9 7TF"/>
    </address>
    -->
</Organization>