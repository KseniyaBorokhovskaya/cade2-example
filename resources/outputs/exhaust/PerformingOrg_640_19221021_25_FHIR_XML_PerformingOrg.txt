<Organization>
    <id value="9e20494b-ddb5-44df-a2ec-293c46c53a17"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Organization-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/ods-organization-code"/>
        <value value="TKD13"/>
    </identifier>
    <!--
    <name value="Midtown District Hospital"/>
    <address>
        <line value="Beckett Street"/>
        <city value="Leeds"/>
        <district value="West YorkShire"/>
        <postalCode value="LS9 7TF"/>
    </address>
    -->
</Organization>