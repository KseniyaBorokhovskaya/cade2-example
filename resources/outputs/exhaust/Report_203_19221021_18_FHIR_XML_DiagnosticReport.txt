<DiagnosticReport xmlns="http://hl7.org/fhir">
    <id value="31ca7006-b6c0-410f-ba30-e2952c8863d2"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-DiagnosticReport-1"/>
    </meta>
    <!--Test Request Summary-->
    <basedOn>
        <reference value="ProcedureRequest/79a072fa-bee7-4dfd-9727-a45c5e119068"/>
    </basedOn>
    <status value="final"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="721981007"/>
            <display value="Diagnostic studies report"/>
        </coding>
    </code>
    <subject>
        <display value="Adams, Alisha"/>
    </subject>
    <issued value="1963-11-17T00:48:00Z"/>
    <performer>
        <actor>
        <reference value="Organization/a4f34df1-fd77-422c-a0e0-0b58e2277e0c"/>

        <display value="Midtown District Hospital"/>
        </actor>
    </performer>
    <result>
        <reference value="Observation/c2093155-4e13-4199-baa4-8134b1fe2859"/>

    </result>
</DiagnosticReport>