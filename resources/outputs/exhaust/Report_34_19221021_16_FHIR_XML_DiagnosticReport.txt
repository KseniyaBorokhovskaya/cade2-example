<DiagnosticReport xmlns="http://hl7.org/fhir">
    <id value="36388a15-2218-408d-bad9-c89eeb107995"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-DiagnosticReport-1"/>
    </meta>
    <!--Test Request Summary-->
    <basedOn>
        <reference value="ProcedureRequest/241bccf8-e1af-4497-a30b-2235a2ce5731"/>
    </basedOn>
    <status value="final"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="721981007"/>
            <display value="Diagnostic studies report"/>
        </coding>
    </code>
    <subject>
        <display value="Storey, Leon"/>
    </subject>
    <issued value="1929-04-16T00:43:00Z"/>
    <performer>
        <actor>
        <reference value="Organization/73b327fd-b1a1-4a53-82d8-ca1ed49178c1"/>

        <display value="Midtown District Hospital"/>
        </actor>
    </performer>
    <result>
        <reference value="Observation/c49e4f6d-623f-4e3a-b76b-68fda61a92f4"/>

    </result>
</DiagnosticReport>