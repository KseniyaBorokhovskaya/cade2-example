<ProcedureRequest>
    <id value="3d86772f-16fe-472d-b646-437b9ab55a3b"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-ProcedureRequest-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="3d86772f-16fe-472d-b646-437b9ab55a3b"/>
    </identifier>
    <status value="active"/>
    <intent value="order"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="26604007"/>
            <display value="FBC - Full blood count"/>
        </coding>
    </code>
    <subject>
        <display value="Chambers, Melissa"/>
    </subject>
    <requester>
        <agent>
            <reference value="Practitioner/79a8cca8-57f5-4375-8ddd-d819977fe5d7"/>

            <display value="Dr Jane Green"/>
        </agent>
    </requester>
    <performer>
        <reference value="Organization/0b82bda2-0967-45fc-a79e-66d67ebea0a6"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <note>
        <text value="FBC"/>
    </note>
</ProcedureRequest>