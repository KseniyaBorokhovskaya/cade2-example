<ProcedureRequest>
    <id value="73ddabd9-e609-466a-9465-0d3f2e96ae52"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-ProcedureRequest-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="73ddabd9-e609-466a-9465-0d3f2e96ae52"/>
    </identifier>
    <status value="active"/>
    <intent value="order"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="26604007"/>
            <display value="FBC - Full blood count"/>
        </coding>
    </code>
    <subject>
        <display value="Sutton, Mohammed"/>
    </subject>
    <requester>
        <agent>
            <reference value="Practitioner/be1b22a6-127f-4340-8756-78135be691f5"/>

            <display value="Dr Jane Green"/>
        </agent>
    </requester>
    <performer>
        <reference value="Organization/e57bebe1-f60a-488c-aed8-d0309dfbf147"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <note>
        <text value="FBC"/>
    </note>
</ProcedureRequest>