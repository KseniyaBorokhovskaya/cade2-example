<ProcedureRequest>
    <id value="d02bcc2a-599c-4d3f-91f7-59df11c0c5f3"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-ProcedureRequest-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="d02bcc2a-599c-4d3f-91f7-59df11c0c5f3"/>
    </identifier>
    <status value="active"/>
    <intent value="order"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="26604007"/>
            <display value="FBC - Full blood count"/>
        </coding>
    </code>
    <subject>
        <display value="Clarke, James"/>
    </subject>
    <requester>
        <agent>
            <reference value="Practitioner/da588497-3aa7-4789-a175-304bfc9fe7d0"/>

            <display value="Dr Jane Green"/>
        </agent>
    </requester>
    <performer>
        <reference value="Organization/caf7844f-8f00-4840-bb4d-f205c91defb0"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <note>
        <text value="FBC"/>
    </note>
</ProcedureRequest>