<ProcedureRequest>
    <id value="6e021f6f-af42-4235-8d8d-7a0af916fc91"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-ProcedureRequest-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="6e021f6f-af42-4235-8d8d-7a0af916fc91"/>
    </identifier>
    <status value="active"/>
    <intent value="order"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="26604007"/>
            <display value="FBC - Full blood count"/>
        </coding>
    </code>
    <subject>
        <display value="Nicholson, Mia"/>
    </subject>
    <requester>
        <agent>
            <reference value="Practitioner/4ef41a9a-07fb-4387-adbc-38da7451e336"/>

            <display value="Dr Jane Green"/>
        </agent>
    </requester>
    <performer>
        <reference value="Organization/dc69c8ab-c7e0-4c34-8a43-4a3763861b38"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <note>
        <text value="FBC"/>
    </note>
</ProcedureRequest>