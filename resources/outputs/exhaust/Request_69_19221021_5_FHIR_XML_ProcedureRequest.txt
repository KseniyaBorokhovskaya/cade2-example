<ProcedureRequest>
    <id value="6240e396-9252-4d9f-9c7e-b0340e7b89b2"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-ProcedureRequest-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="6240e396-9252-4d9f-9c7e-b0340e7b89b2"/>
    </identifier>
    <status value="active"/>
    <intent value="order"/>
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="26604007"/>
            <display value="FBC - Full blood count"/>
        </coding>
    </code>
    <subject>
        <display value="Davies, Anna"/>
    </subject>
    <requester>
        <agent>
            <reference value="Practitioner/f1ecca12-e57a-473a-b821-a6cd808526ab"/>

            <display value="Dr Jane Green"/>
        </agent>
    </requester>
    <performer>
        <reference value="Organization/5462dff7-e9a9-4982-a0f2-2d094f065f8c"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <note>
        <text value="FBC"/>
    </note>
</ProcedureRequest>