<Practitioner>
    <id value="da588497-3aa7-4789-a175-304bfc9fe7d0"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Practitioner-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/sds-user-id"/>
        <value value="G6040649"/>
    </identifier>
    <name>
        <text value="Dr Jane Green"/>
    </name>
</Practitioner>