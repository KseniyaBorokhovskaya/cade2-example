<Practitioner>
    <id value="91a0f210-9596-4619-b4c9-2cc29b20de64"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Practitioner-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/sds-user-id"/>
        <value value="G6040649"/>
    </identifier>
    <name>
        <text value="Dr Jane Green"/>
    </name>
</Practitioner>