<Practitioner>
    <id value="daa38e87-1c4b-4a87-abe1-b79873b9c4d4"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Practitioner-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/sds-user-id"/>
        <value value="G6040649"/>
    </identifier>
    <name>
        <text value="Dr Jane Green"/>
    </name>
</Practitioner>