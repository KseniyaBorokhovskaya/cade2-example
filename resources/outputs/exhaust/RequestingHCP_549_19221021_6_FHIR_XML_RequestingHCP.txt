<Practitioner>
    <id value="ad28816b-48c3-476b-bef0-23872e5f527b"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Practitioner-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/sds-user-id"/>
        <value value="G6040649"/>
    </identifier>
    <name>
        <text value="Dr Jane Green"/>
    </name>
</Practitioner>