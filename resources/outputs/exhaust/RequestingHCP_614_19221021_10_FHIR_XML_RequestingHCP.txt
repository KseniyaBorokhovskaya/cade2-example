<Practitioner>
    <id value="0ea81ad0-e69b-4f76-bb29-46f5271941f2"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Practitioner-1"/>
    </meta>
    <identifier>
        <system value="https://fhir.nhs.uk/Id/sds-user-id"/>
        <value value="G6040649"/>
    </identifier>
    <name>
        <text value="Dr Jane Green"/>
    </name>
</Practitioner>