<Specimen>
    <id value="ea3ff084-7904-484d-930b-4b4b4ab702a7"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="ea3ff084-7904-484d-930b-4b4b4ab702a7"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Norton, Isabel"/>
    </subject>
    <receivedTime value="1979-11-02T00:16:00Z"/>
    <collection>
        <collectedDateTime value="1979-11-02T00:16:00Z"/>
    </collection>
</Specimen>