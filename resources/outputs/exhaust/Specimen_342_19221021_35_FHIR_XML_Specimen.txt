<Specimen>
    <id value="52ab9402-bf09-42d9-946c-176f4cb45f55"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="52ab9402-bf09-42d9-946c-176f4cb45f55"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Hayes, Amy"/>
    </subject>
    <receivedTime value="1982-02-25T00:10:00Z"/>
    <collection>
        <collectedDateTime value="1982-02-25T00:10:00Z"/>
    </collection>
</Specimen>