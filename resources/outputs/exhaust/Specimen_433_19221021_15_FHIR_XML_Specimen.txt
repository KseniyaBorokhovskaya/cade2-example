<Specimen>
    <id value="13130945-b66b-47bb-a7a1-cda4baca4769"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="13130945-b66b-47bb-a7a1-cda4baca4769"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Briggs, Lola"/>
    </subject>
    <receivedTime value="1991-03-04T00:17:00Z"/>
    <collection>
        <collectedDateTime value="1991-03-04T00:17:00Z"/>
    </collection>
</Specimen>