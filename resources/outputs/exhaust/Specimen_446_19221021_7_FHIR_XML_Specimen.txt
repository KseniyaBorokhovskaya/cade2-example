<Specimen>
    <id value="43766367-a1e3-42cc-836b-a4f2e1e9d239"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="43766367-a1e3-42cc-836b-a4f2e1e9d239"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Bradley, Bailey"/>
    </subject>
    <receivedTime value="1991-09-20T00:21:00Z"/>
    <collection>
        <collectedDateTime value="1991-09-20T00:21:00Z"/>
    </collection>
</Specimen>