<Specimen>
    <id value="6e2cd79e-7b3a-414d-864a-0cbf1e5b98bf"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="6e2cd79e-7b3a-414d-864a-0cbf1e5b98bf"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Cole, Alexandra"/>
    </subject>
    <receivedTime value="2020-03-12T00:48:00Z"/>
    <collection>
        <collectedDateTime value="2020-03-12T00:48:00Z"/>
    </collection>
</Specimen>