<Specimen>
    <id value="94a3959f-2608-4a0a-a356-2212f321ad73"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Specimen-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="94a3959f-2608-4a0a-a356-2212f321ad73"/>
    </identifier>
    <status value="available"/>
    <type>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="53130003"/>
            <display value="Venous blood (substance)"/>
        </coding>
    </type>
    <subject>
        <display value="Henry, Noah"/>
    </subject>
    <receivedTime value="1952-05-24T00:44:00Z"/>
    <collection>
        <collectedDateTime value="1952-05-24T00:44:00Z"/>
    </collection>
</Specimen>