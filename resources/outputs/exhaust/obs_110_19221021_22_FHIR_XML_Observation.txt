<Observation>
    <id value="362d14a6-ee96-4a49-aaaa-85d9154b022f"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="362d14a6-ee96-4a49-aaaa-85d9154b022f"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="993481000000101"/>
            <display value="Mean platelet volume"/>
        </coding>
    </code>
    <subject>
        <display value="Fisher, Jacob"/>
    </subject>
    <effectiveDateTime value="1955-03-14T00:40:00Z"/>
    <performer>
        <reference value="Organization/897b8b91-3abd-4d27-9aaa-61c9588e9b07"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="13.0"/>
        <unit value="fL as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="fL"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/414379de-fef8-4455-8354-57a5a8063952"/>

    </specimen>
</Observation>