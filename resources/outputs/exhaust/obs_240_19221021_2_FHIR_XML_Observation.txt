<Observation>
    <id value="e066c50a-e4d1-4e2e-b992-27acffa9e02d"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="e066c50a-e4d1-4e2e-b992-27acffa9e02d"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1107811000000100"/>
            <display value="Prostate specific antigen mass concentration in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Rees, Naomi"/>
    </subject>
    <effectiveDateTime value="1977-07-20T00:50:00Z"/>
    <performer>
        <reference value="Organization/d833a30a-7b19-4d36-8b8c-e95e6a72cb9b"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="5.9"/>
        <unit value="ug/L as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="ug/L"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/4eefada7-7511-4d9f-ab5e-3d1338d2eae1"/>

    </specimen>
</Observation>