<Observation>
    <id value="a9d87a12-8e0d-42b5-afbb-271b04cbaee1"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="a9d87a12-8e0d-42b5-afbb-271b04cbaee1"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1109401000000100"/>
            <display value="Free T4 (thyroxine) mass concentration in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Wallace, Tegan"/>
    </subject>
    <effectiveDateTime value="1980-12-03T01:15:00Z"/>
    <performer>
        <reference value="Organization/c1a8390e-7c09-4906-9cb4-a1ea09fbc339"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="19.0"/>
        <unit value="pmol/L as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="pmol/L"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/7d11810d-2f8d-4716-a251-73b43090c0e1"/>

    </specimen>
</Observation>