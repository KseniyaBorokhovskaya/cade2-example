<Observation>
    <id value="307692b4-2669-4ff8-8d4a-ac712d816cd9"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="307692b4-2669-4ff8-8d4a-ac712d816cd9"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1107681000000100"/>
            <display value="HDL (high density lipoprotein) cholesterol substance concentration in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Watkins, Kiera"/>
    </subject>
    <effectiveDateTime value="1982-01-29T00:47:00Z"/>
    <performer>
        <reference value="Organization/31272716-bb88-4ff6-a582-56c134fc7387"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="0.8"/>
        <unit value="mmol/L as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="mmol/L"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/26fef16b-d87d-4532-b39a-03b104fcba91"/>

    </specimen>
</Observation>