<Observation>
    <id value="26d75be7-3f72-4640-8224-096bdea93c77"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="26d75be7-3f72-4640-8224-096bdea93c77"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1107761000000100"/>
            <display value="Potassium substance concentration in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Adams, Rhys"/>
    </subject>
    <effectiveDateTime value="1989-08-09T00:49:00Z"/>
    <performer>
        <reference value="Organization/a3ed49fe-2ab3-4bc4-988a-ecf1520e2261"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="4.5"/>
        <unit value="mmol/L as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="mmol/L"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/a7bfd048-6e13-433e-aa57-5c821cb5ae8a"/>

    </specimen>
</Observation>