<Observation>
    <id value="7a38aabd-d2a2-4118-9c87-138c1b4c8416"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="7a38aabd-d2a2-4118-9c87-138c1b4c8416"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1107741000000100"/>
            <display value="HDL (high density lipoprotein) cholesterol/total cholesterol ratio in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Young, Scott"/>
    </subject>
    <effectiveDateTime value="1993-07-19T00:34:00Z"/>
    <performer>
        <reference value="Organization/3adcb7e5-32ed-4647-a461-fb0fb795d149"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="9.6"/>
        <unit value="ratio as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="{ratio}"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/4a1bd2e6-c5f4-4c19-b0c1-93ac62a3fb6b"/>

    </specimen>
</Observation>