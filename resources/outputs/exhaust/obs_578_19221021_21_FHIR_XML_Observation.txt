<Observation>
    <id value="ae911841-c58c-42e9-a466-c08d85e43397"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="ae911841-c58c-42e9-a466-c08d85e43397"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="993501000000105"/>
            <display value="Red blood cell distribution width"/>
        </coding>
    </code>
    <subject>
        <display value="Nicholson, Mia"/>
    </subject>
    <effectiveDateTime value="2015-04-06T00:28:00Z"/>
    <performer>
        <reference value="Organization/dc69c8ab-c7e0-4c34-8a43-4a3763861b38"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="14"/>
        <unit value="% as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="%"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/1289afaf-f845-4b3b-be7b-059abfa7c081"/>

    </specimen>
</Observation>