<Observation>
    <id value="f6374469-57c2-4789-badf-48ca34ee2fa0"/>
    <meta>
        <profile value="https://fhir.hl7.org.uk/STU3/StructureDefinition/CareConnect-Observation-1"/>
    </meta>
    <identifier>
        <system value="https://tools.ietf.org/html/rfc4122"/>
        <value value="f6374469-57c2-4789-badf-48ca34ee2fa0"/>
    </identifier>
    <status value="final"/>
    <category>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="394595002"/>
            <display value="Pathology (qualifier value)"/>
        </coding>
    </category>
    <!--What is being tested-->
    <code>
        <coding>
            <system value="http://snomed.info/sct"/>
            <code value="1106511000000100"/>
            <display value="Chloride substance concentration in serum"/>
        </coding>
    </code>
    <subject>
        <display value="Davies, Anna"/>
    </subject>
    <effectiveDateTime value="1950-04-02T00:39:00Z"/>
    <performer>
        <reference value="Organization/5462dff7-e9a9-4982-a0f2-2d094f065f8c"/>

        <display value="Midtown District Hospital"/>
    </performer>
    <valueQuantity>
        <value value="105"/>
        <unit value="mmol/L as text"/>
        <system value="http://unitsofmeasure.org"/>
        <code value="mmol/L"/>
    </valueQuantity>
    <specimen>
        <reference value="Specimen/3796118f-aff3-4404-a681-0dc328e8a28c"/>

    </specimen>
</Observation>